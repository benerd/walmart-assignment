function greySkull(str) {
  let formatters = [
    {
      pattern: /[a,A]/g,
      replacement: 4
    },
    {
      pattern: /[e,E]/g,
      replacement: 3
    },
    {
      pattern: /[i,I]/g,
      replacement: 1
    },
    {
      pattern: /[o,O]/g,
      replacement: 0
    },
    {
      pattern: /[s,S]/g,
      replacement: 5
    },
    {
      pattern: /[t,T]/g,
      replacement: 7
    },
    {
      pattern: /[b,D]/g,
      replacement: 5
    }
  ];

  for (let i = 0; i < formatters.length; i++) {
    str = str.replace(formatters[i].pattern, formatters[i].replacement);
  }
  return str;
}

greySkull("C is for cookie, that's good enough for me");
