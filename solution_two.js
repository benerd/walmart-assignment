function charCount(str) {
  let char = str[0];
  let count = 1;
  let result = [];
  for (let i = 0; i <= str.length; i++) {
    if (char === str[i]) {
      if (!result.includes(str[i])) {
        result.push(str[i]);
      } else {
        ++count;
      }
    } else {
      result.push(count);
      result.push(str[i]);
      char = str[i];
      count = 1;
    }
  }
  return result.join('');
}

charCount('aabbaa');
